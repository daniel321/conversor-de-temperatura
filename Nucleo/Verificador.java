package Nucleo;

public class Verificador {
    public Verificador(){
        
    }
    Conversor Converter = new Conversor();
    
    public double Temperatura(String De,String Para,double Temperatura){
        switch(De){
            case "Celsius":
                switch(Para){
                    case "Fahrenheit":
                        return Converter.Celsius2Fahrenheit(Temperatura);
                    case "Kelvin":
                        return Converter.Celsius2Kelvin(Temperatura);
                
                }
            break;
            case "Fahrenheit":
                switch(Para){
                    case "Celsius":
                        return Converter.Fahrenheit2Celsius(Temperatura);
                    case "Kelvin":
                        return Converter.Fahrenheit2Kelvin(Temperatura);
                
                }
            break;
            case "Kelvin":
                switch(Para){
                    case "Fahrenheit":
                        return Converter.Kelvin2Fahrenheit(Temperatura);
                    case "Celsius":
                        return Converter.Kelvin2Celsius(Temperatura);
                
                }
        }
        return 0;
    }
    
}
