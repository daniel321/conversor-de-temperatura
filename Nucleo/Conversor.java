package Nucleo;

public class Conversor {
    
    private double Celsius;
    private double Fahrenheit;
    private double Kelvin;
    
    public Conversor(){
        
    }
    
    public double Celsius2Kelvin(double temperatura){
        return temperatura + 273;
    }
    public double Celsius2Fahrenheit(double temperatura){
        return temperatura*1.8 + 32;
    }
    public double Kelvin2Celsius(double temperatura){
        return temperatura - 273;
    }
    public double Kelvin2Fahrenheit(double temperatura){
        return (temperatura+273)*1.8 + 32;
    }
    public double Fahrenheit2Celsius(double temperatura){
        return temperatura*1.8 + 32;
    }
    public double Fahrenheit2Kelvin(double temperatura) {
        return temperatura*1.8 + 32 + 273;
    }
}
    
