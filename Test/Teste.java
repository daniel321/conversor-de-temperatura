package Test;
import Nucleo.Conversor;

import static org.junit.Assert.*;
import model.Conversor;
import org.junit.Before;
import org.junit.Test;

public class Teste {

    private Conversor Converter;
    
    @Before
    public void setUp() throws Exception {
        Converter = new Conversor();
    }
    
    @Test
    public void testCelsiusParaFahrenheitComValorDouble() {
         assertEquals(104.0, Converter.Celsius2Fahrenheit(40.0),0.01);
    }
    @Test
    public void testFahrenheitParaCelsiusComValorDouble(){
        assertEquals(-15.0, Converter.Fahrenheit2Celsius(5.0), 1);
    }

    @Test
    public void testKelvinParaCelsiusComValorDouble(){
        assertEquals(-223, Converter.Kelvin2Celsius(50), 1);
    }

    @Test
    public void testCelsiusParaKelvinComValorDouble(){
        assertEquals(15, Converter.Celsius2Kelvin(-258.15), 1);
    }

    @Test
    public void testKelvinParaFahrenheitComValorDouble(){
        assertEquals(15, Converter.Kelvin2Fahrenheit(263.705556), 1);
    }

    @Test
    public void testFahrenheitParaKelvinComValorDouble(){
        assertEquals(263.705556, Converter.Fahrenheit2Kelvin(15), 1);

}
}